package fr.univlille.iut.info.r402;

import static org.junit.jupiter.api.Assertions.assertEquals;

import fr.univlille.iut.info.src.r402.Stock;
import fr.univlille.iut.info.src.r402.MoneyCurrency;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class StockStepsdefs{

    Stock stock;

    @When("créer un stock")
    public void créerUneAction(){
        stock = new Stock(5.0,MoneyCurrency.EUR);
    } 

    @Then("cette action apparait et a les infos que je lui ait donné a la création")
    public void actionExiste(){
        assertEquals(stock.getValue(), 5.0);
        assertEquals(stock.getCurrency(), MoneyCurrency.EUR);
    }

    @When("j'actualise la valeur d'un stock")
    public void modifieValeur(){
        stock.setValue(6.0);
    }

    @Then("la nouvelle valeur est enregistré")
    public void testValeur(){
        assertEquals(stock.getValue(), 6.0);
    }

}
