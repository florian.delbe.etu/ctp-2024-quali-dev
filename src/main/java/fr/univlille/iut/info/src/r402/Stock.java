package fr.univlille.iut.info.src.r402;

public class Stock {
    private double value;
    private MoneyCurrency currency;

    public Stock(double value,MoneyCurrency moneyCurrency){
        this.value=value;
        this.currency=moneyCurrency;
    }

    public double getValue() {
        return value;
    }

    public MoneyCurrency getCurrency() {
        return currency;
    }

    public void setValue(double value) {
        this.value = value;
    }

    
}
